FROM registry.hzdr.de/hzb/epics/base/ubuntu_20_04:1.0.0 

# create directories
RUN mkdir -p /opt/epics/support
RUN mkdir -p /opt/epics/ioc

# Define build arguments
ARG MODULES_YAML=modules.yml

# Install required modules based on modules.yml
RUN apt-get update && apt-get install -y git build-essential python3-pip
RUN pip install pyyaml

COPY $MODULES_YAML /tmp/modules.yml

# Run Python script to generate module list
COPY generate_modules.py /tmp/generate_modules.py
RUN python3 /tmp/generate_modules.py

## Copy the updated shell script
COPY install_modules.sh /tmp/install_modules.sh

# Change permissions for the script
RUN chmod +x /tmp/install_modules.sh

# Execute the script using WORKDIR
WORKDIR /tmp
RUN /bin/bash -c ./install_modules.sh

# Install needed packages
RUN apt install curl -y 
RUN curl -L https://github.com/mikefarah/yq/releases/download/v4.13.4/yq_linux_amd64 -o /usr/local/bin/yq && chmod +x /usr/local/bin/yq

# Copy install IOC script to temp
COPY install_ioc.sh /tmp/install_ioc.sh
# Change permissions for the script
RUN chmod +x /tmp/install_ioc.sh

# Execute the script
RUN /bin/bash -c /tmp/install_ioc.sh

# Cleanup
RUN rm /tmp/modules.yml /tmp/modules_to_install.txt

# create a directory to store protocol files
RUN mkdir /opt/epics/ioc/protocols

# create a directory to store pv names
RUN mkdir /opt/epics/ioc/log && touch /opt/epics/ioc/log/pv.dbl


